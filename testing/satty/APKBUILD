# Contributor: Sodface <sod@sodface.com>
# Maintainer: Sodface <sod@sodface.com>
pkgname=satty
pkgver=0.8.2
pkgrel=0
pkgdesc="Screenshot annotation tool"
url="https://github.com/gabm/Satty"
arch="all"
license="MPL-2.0"
makedepends="
	cargo
	cargo-auditable
	gtk4.0-dev
	libadwaita-dev
	"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
options="!check" # no test suite
source="$pkgname-$pkgver.tar.gz::https://github.com/gabm/Satty/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/Satty-$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

package() {
	install -Dm755 target/release/satty -t "$pkgdir"/usr/bin

	install -Dm644 satty.desktop \
		-t "$pkgdir"/usr/share/applications/
	install -Dm644 assets/satty.svg \
		-t "$pkgdir"/usr/share/icons/hicolor/scalable/apps

	install -Dm644 completions/satty.bash \
		"$pkgdir"/usr/share/bash-completion/completions/satty
	install -Dm644 completions/satty.fish \
		-t "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 completions/_satty \
		-t "$pkgdir"/usr/share/zsh/site-functions
}

sha512sums="
71386f8615c233b8c184981b442874967e409f5029a2259f77e2124691b464973694b0018b232e8e4bcdb64d8c622994054a75c2e2ea46ea5768d5ad964e8292  satty-0.8.2.tar.gz
"
