# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=28.0.5
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-Iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaAile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaSlab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaCurly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaCurlySlab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/Iosevka-*.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/IosevkaAile-*.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/IosevkaSlab-*.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/IosevkaCurly-*.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/IosevkaCurlySlab-*.ttc
}

sha512sums="
ebf1e536c6bf934e26b3d9046d8dd7e706d1a86bea9bcf9c02fe7bdb0c8aae835ba2780aea19a36145d5e024bff15d24872d8161da560c16be3c8e2e8d9f8784  PkgTTC-Iosevka-28.0.5.zip
6b3a55aa67d806b92231b7a364e523743948e94d9f192ab45722bc79a3f4d8fe03987b1c21eec5d1507f6875041770293f53673318509e59422fe65a1109a317  PkgTTC-IosevkaAile-28.0.5.zip
dc32ffaa6ddf45fecec66101de8311a0cd91b3c14f832bc5f6b19f0b50f32ba408e7dbf1e666b50b714ac6a207bd4b24d30844e0eb4a4a225187ad92c5272906  PkgTTC-IosevkaSlab-28.0.5.zip
50d4f0f131ece415004e704170ad9ef970b3d5cbee0f0e2c44d0d9f4479b0ab5c0400c9e4c449c4aa7a00280ab95582c25d9e013b3fa35dccb9996f5bbc484a4  PkgTTC-IosevkaCurly-28.0.5.zip
3143c6e5ece318c76bc96a6d2e27a2762c1789460e8fe981aab2196437ded23e19cbe639a9d558e23a382697d551b251dd25837fdd4f85e916e420353188daec  PkgTTC-IosevkaCurlySlab-28.0.5.zip
"
