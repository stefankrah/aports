# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=rqlite
pkgver=8.16.7
pkgrel=0
pkgdesc="Lightweight, distributed relational database built on SQLite"
url="https://github.com/rqlite/rqlite"
arch="all"
license="MIT"
makedepends="go sqlite-dev"
checkdepends="python3 py3-requests"
options="chmod-clean net !check" # a bunch of them fail due to runtime requirements
subpackages="
	$pkgname-doc
	$pkgname-bench
	$pkgname-client
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rqlite/rqlite/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build ./cmd/rqbench &
	go build ./cmd/rqlite &
	go build ./cmd/rqlited &
	wait
}

check() {
	RQLITED_PATH="$builddir"/rqlited python3 system_test/full_system_test.py
}

package() {
	install -Dm755 rqbench rqlite rqlited \
		-t "$pkgdir"/usr/bin/

	install -Dm644 DOC/*.md \
		-t "$pkgdir"/usr/share/doc/$pkgname/

	install -Dm644 LICENSE \
		-t "$pkgdir"/usr/share/licenses/$pkgname/
}

bench() {
	pkgdesc="$pkgdesc (benchmark utility)"

	amove usr/bin/rqbench
}

client() {
	pkgdesc="$pkgdesc (client)"

	amove usr/bin/rqlite
}

sha512sums="
d9abfdaf546d082f9c0e472c55093f78fd52882279998eca2bd1a634ca965e8315f6b677f7373862517fe9d3970a4b096cbf8ece758a534292d978dfe5c2f72c  rqlite-8.16.7.tar.gz
"
