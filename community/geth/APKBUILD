# Contributor: André Klitzing <aklitzing@gmail.com>
# Maintainer: André Klitzing <aklitzing@gmail.com>
pkgname=geth
pkgver=1.13.10
pkgrel=0
pkgdesc="Official Go implementation of the Ethereum protocol"
url="https://geth.ethereum.org/"
arch="all"
license="LGPL-3.0-or-later"
makedepends="go linux-headers"
checkdepends="fuse"
options="!check chmod-clean net"
source="$pkgname-$pkgver.tar.gz::https://github.com/ethereum/go-ethereum/archive/v$pkgver.tar.gz"
builddir="$srcdir/go-ethereum-$pkgver"

# secfixes:
#   1.10.22-r0:
#     - CVE-2022-37450

export GOROOT=/usr/lib/go
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make
}

check() {
	make test
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	install -m755 -t "$pkgdir"/usr/bin build/bin/*
}

sha512sums="
36e26634c4c18be4cf5aa92babf4e28073c5be3bd46c069520cb605c94dbb33cb2ebf60f86d23b160b268c58737ad05aab1de3e2b59044865884729d4a9d55ad  geth-1.13.10.tar.gz
"
