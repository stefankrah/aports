# Maintainer: Jeff Dickey <alpine@mise.jdx.dev>
pkgname=mise
pkgver=2024.1.18
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://mise.jdx.dev"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
subpackages="$pkgname-doc"
provides="rtx=$pkgver-r$pkgrel"
replaces="rtx"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdx/mise/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin mise
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mise -t "$pkgdir/usr/bin/"
	install -Dm644 README.md docs/*.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm644 "man/man1/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
25f712d44199c47872a42a835140afde77428596cf627e1d8ea32dc0cdb1898dd209e7817e17ed379a24325d00633855d923264278742454a0b4a434399645bd  mise-2024.1.18.tar.gz
"
