# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=cargo-make
pkgver=0.37.7
pkgrel=0
pkgdesc="Rust task runner and build tool"
url="https://github.com/sagiegurari/cargo-make"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="Apache-2.0"
makedepends="cargo openssl-dev cargo-auditable"
subpackages="$pkgname-bash-completion"
source="https://github.com/sagiegurari/cargo-make/archive/$pkgver/cargo-make-$pkgver.tar.gz"
options="!check"  # FIXME: some tests are broken

_cargo_opts="--frozen --no-default-features --features tls-native"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	install -D -m755 -t "$pkgdir"/usr/bin/ \
		target/release/cargo-make \
		target/release/makers

	install -D -m644 extra/shell/makers-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/makers
}

sha512sums="
a4746052253604d3da88f1c4b019777ef28712b4a1b8a475c379f1bc7f1ff98fe444c7e3cd9aed8805e695aeb2ea6457527a96392e2421b323bfad25c55cf635  cargo-make-0.37.7.tar.gz
"
