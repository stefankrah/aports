# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gjs
# even number after first dot are the stable releases
pkgver=1.78.2
pkgrel=0
pkgdesc="GNOME javascript library"
url="https://wiki.gnome.org/Projects/Gjs"
# armhf and s390x blocked by mozjs102
arch="all !armhf !s390x"
license="MIT AND LGPL-2.0-or-later"
makedepends="
	cairo-dev
	clang
	dbus
	gobject-introspection-dev
	gtk+3.0-dev
	libffi>=3.3
	meson
	mozjs115-dev
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev"
source="https://download.gnome.org/sources/gjs/${pkgver%.*}/gjs-$pkgver.tar.xz
	encoding.patch
	"

case "$CARCH" in
riscv64)
	# lld broken on riscv64
	;;
*)
	makedepends="$makedepends lld"
	;;
esac

build() {
	case "$CARCH" in
	aarch64|arm*|riscv64)
		# not supported by clang here
		export CFLAGS="${CFLAGS/-fstack-clash-protection}"
		export CXXFLAGS="${CXXFLAGS/-fstack-clash-protection}"
		;;
	esac

	case "$CARCH" in
	riscv64)
		local lto=false
		;;
	*)
		local lto=true
		export LDFLAGS="$LDFLAGS -fuse-ld=lld"
		;;
	esac

	CC=clang \
	CXX=clang++ \
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	abuild-meson \
		--buildtype=release \
		-Db_lto=$lto \
		-Db_ndebug=true \
		-Dprofiler=disabled \
		-Dinstalled_tests=false \
		output
	meson compile -C output
}

check() {
	# Tests can take a while on armv7
	xvfb-run -a meson test --print-errorlogs --no-rebuild -C output -t 10
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
4becf2085274a42d5e03c3ba6f07953b2a17e5678b951d30216e842ef05a0d1f1989bd8357bfb4429b96fbb8302e2d278b6279edb3005d6d88a30daa547e3781  gjs-1.78.2.tar.xz
3524a4c6772f1be1d6e2320650d7fb9f81cc2ceb7c79c2521c0ee7a4202d5681a88586ab9439050611ee1719f9a977bb65d0eaaa536148e7e4590baf1f50eae8  encoding.patch
"
